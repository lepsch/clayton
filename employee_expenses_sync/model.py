from sqlalchemy import Column, Integer, String, DateTime, Numeric, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class Employee(Base):
    __tablename__ = 'employee'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    cpf = Column(String, nullable=False)
    role = Column(String)
    admission_date = Column(DateTime, nullable=False)
    payrolls = relationship('Payroll', back_populates='employee')


class Payroll(Base):
    __tablename__ = 'payroll'

    id = Column(Integer, primary_key=True)
    year = Column(Integer, nullable=False)
    month = Column(Integer, nullable=False)
    wage = Column(Numeric, nullable=False)
    employee_id = Column(Integer, ForeignKey('employee.id'))
    employee = relationship('Employee', back_populates='payrolls')
